package idealista.java.misc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ReadingFiles {

	private static final Path PATH = Paths.get("src", "main", "res", "file.txt");

	public static void main(String[] args) throws IOException {
		try(Stream<String> lines = Files.lines(PATH)) {
			Predicate<String> startsWithSlash = l -> l.startsWith("/");
			Predicate<String> startsWithStar = l -> l.startsWith("*");
			Predicate<String> isEmpty = String::isEmpty;
			Predicate<String> isCode = startsWithSlash.or(startsWithStar).or(isEmpty).negate();
			lines
					.filter(l -> isCode.test(l.trim()))
					.forEach(System.out::println);
		}
	}
}
