package idealista.java.gof.templatemethod;


import java.util.function.Consumer;

public class LambdaStyle {

    static void execute(Consumer<Resource> consumer) {
        Resource resource = new Resource();
        try {
            consumer.accept(resource);
        } finally {
            resource.dispose();
        }
    }

    public static void main(String[] args) {
        execute(Resource::useResource);
        execute(Resource::employResource);
    }
}
