package idealista.java.gof.decorator;

import java.util.function.DoubleUnaryOperator;
import java.util.stream.Stream;

public class LambdaStyle {

    public static class DefaultSalaryCalculator implements DoubleUnaryOperator {

        @Override
        public double applyAsDouble(double grossAnnual) {
            return grossAnnual / 12;
        }
    }

    public static void main(String[] args) {
        //general, regional, health
        System.out.println(
                new DefaultSalaryCalculator()
                .andThen(Taxes::generalTax)
                .andThen(Taxes::regionalTax)
                .andThen(Taxes::healthInsurance)
                .applyAsDouble(30000.0)
        );

        System.out.println(
                calculate(30000.0, d -> d/12,
                        Taxes::generalTax,
                        Taxes::regionalTax,
                        Taxes::healthInsurance)
        );
    }

    static Double calculate(double gross, DoubleUnaryOperator ...operators) {
       return  Stream.of(operators)
               .reduce(DoubleUnaryOperator.identity(),
                DoubleUnaryOperator::andThen)
                .applyAsDouble(gross);
    }


}
