package idealista.java.gof.visitor;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class LambdaVisitor {

	static class Visitor<A> implements Function<Object, A> {

		private final Map<Class<?>, Function<Object, A>> map = new HashMap<>();

		<B> Acceptor<A, B> on(Class<B> clazz) {
			return new Acceptor<>(this, clazz);
		}

		@Override
		public A apply(Object o) {
			return map.get(o.getClass()).apply(o);
		}

		static class Acceptor<A, B> {
			private final Visitor visitor;
			private final Class<B> clazz;

			public Acceptor(Visitor<A> visitor, Class<B> clazz) {
				this.visitor = visitor;
				this.clazz = clazz;
			}

			Visitor<A> then(Function<B, A> f) {
				visitor.map.put(clazz, f);
				return visitor;
			}
		}
	}

	static class Square {
		final double side;

		Square(double side) {
			this.side = side;
		}
	}

	static class Circle {
		final double radius;

		Circle(double radius) {
			this.radius = radius;
		}
	}

	static class Rectangle {
		final double width;
		final double height;

		Rectangle(double width, double height ) {
			this.width = width;
			this.height = height;
		}
	}

	private static final Visitor<Double> areaVisitor = new Visitor<Double>()
			.on(Square.class).then(s -> s.side * s.side)
			.on(Rectangle.class).then(r -> r.width * r.height)
			.on(Circle.class).then(c -> Math.PI * c.radius * c.radius);

	public static void main(String[] args) {
		Double rectangleArea = areaVisitor.apply(new Rectangle(3, 4));
		System.out.println(rectangleArea);
	}
}
